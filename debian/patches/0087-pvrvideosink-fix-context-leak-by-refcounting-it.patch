From 344730c4808d33b1c43768e5d8876cac448e3553 Mon Sep 17 00:00:00 2001
From: Vincent Penquerc'h <vincent.penquerch@collabora.co.uk>
Date: Thu, 21 Jun 2012 18:26:34 +0000
Subject: [PATCH] pvrvideosink: fix context leak by refcounting it

---
 sys/pvr2d/gstpvrbufferpriv.c |   12 +++++++-
 sys/pvr2d/gstpvrbufferpriv.h |    1 +
 sys/pvr2d/gstpvrvideosink.c  |   62 +++++++++++++++++++++++++++++++++---------
 sys/pvr2d/gstpvrvideosink.h  |   13 ++++++++-
 4 files changed, 73 insertions(+), 15 deletions(-)

diff --git a/sys/pvr2d/gstpvrbufferpriv.c b/sys/pvr2d/gstpvrbufferpriv.c
index 03efba2..dda1f62 100644
--- a/sys/pvr2d/gstpvrbufferpriv.c
+++ b/sys/pvr2d/gstpvrbufferpriv.c
@@ -34,6 +34,9 @@
 #include "gstpvrvideosink.h"
 #include "gstpvrbufferpriv.h"
 
+#define LINUX
+#include <dri2_omap_ws.h>
+
 #define GST_PVR_BUFFER_PRIV_QUARK gst_pvr_buffer_priv_quark_get_type()
 static GST_BOILERPLATE_QUARK (GstPVRBufferPriv, gst_pvr_buffer_priv_quark);
 
@@ -71,6 +74,7 @@ gst_pvr_buffer_priv_finalize (GstMiniObject * mini_obj)
   GstPVRBufferPriv *priv = (GstPVRBufferPriv *) mini_obj;
 
   PVR2DMemFree (priv->context, priv->mem_info);
+  gst_display_handle_unref (priv->display_handle);
 }
 
 static void
@@ -98,7 +102,10 @@ gst_pvr_buffer_priv (GstPVRVideoSink * sink, GstBuffer * buf)
 
     priv = (GstPVRBufferPriv *) gst_mini_object_new (GST_TYPE_PVR_BUFFER_PRIV);
 
-    priv->context = sink->dcontext->pvr_context;
+    priv->display_handle =
+        gst_display_handle_ref (sink->dcontext->gst_display_handle);
+    priv->context =
+        ((DRI2Display *) priv->display_handle->display_handle)->hContext;
     if (PVR2DImportDmaBuf (priv->context, gst_dma_buf_get_fd (dmabuf),
             &priv->mem_info)) {
       GST_ERROR_OBJECT (sink, "could not import bo: %s", strerror (errno));
@@ -107,6 +114,9 @@ gst_pvr_buffer_priv (GstPVRVideoSink * sink, GstBuffer * buf)
     }
 
     set_pvr_buffer_priv (buf, priv);
+
+    /* set_pvr_buffer_priv will ref priv via gst_structure_id_new */
+    gst_mini_object_unref (GST_MINI_OBJECT_CAST (priv));
   }
   return priv;
 }
diff --git a/sys/pvr2d/gstpvrbufferpriv.h b/sys/pvr2d/gstpvrbufferpriv.h
index 41a0aaf..5aa3999 100644
--- a/sys/pvr2d/gstpvrbufferpriv.h
+++ b/sys/pvr2d/gstpvrbufferpriv.h
@@ -54,6 +54,7 @@ struct _GstPVRBufferPriv
   GstMiniObject parent;
 
   PVR2DCONTEXTHANDLE context;
+  GstDisplayHandle *display_handle;
   PVR2DMEMINFO *mem_info;
 };
 
diff --git a/sys/pvr2d/gstpvrvideosink.c b/sys/pvr2d/gstpvrvideosink.c
index 9acc76b..74b25b5 100644
--- a/sys/pvr2d/gstpvrvideosink.c
+++ b/sys/pvr2d/gstpvrvideosink.c
@@ -109,6 +109,33 @@ static GstVideoSinkClass *parent_class = NULL;
 #define GST_PVRVIDEO_BUFFER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_PVRVIDEO_BUFFER, GstPVRVideoBufferClass))
 
 
+static GstDisplayHandle *
+gst_display_handle_new (WSEGLDrawableHandle dh, const WSEGL_FunctionTable * ft)
+{
+  GstDisplayHandle *display_handle = g_slice_new0 (GstDisplayHandle);
+  display_handle->refcount = 1;
+  display_handle->wsegl_table = ft;
+  display_handle->display_handle = dh;
+  return display_handle;
+}
+
+GstDisplayHandle *
+gst_display_handle_ref (GstDisplayHandle * display_handle)
+{
+  g_atomic_int_inc (&display_handle->refcount);
+  return display_handle;
+}
+
+void
+gst_display_handle_unref (GstDisplayHandle * display_handle)
+{
+  if (g_atomic_int_dec_and_test (&display_handle->refcount)) {
+    display_handle->wsegl_table->pfnWSEGL_CloseDisplay (display_handle->
+        display_handle);
+    g_slice_free (GstDisplayHandle, display_handle);
+  }
+}
+
 static const char *
 pvr2dstrerr (PVR2DERROR err)
 {
@@ -220,8 +247,8 @@ pvr_recreate_drawable (GstPVRVideoSink * pvrvideosink)
 
   if (dcontext->drawable_handle) {
     glerror =
-        dcontext->wsegl_table->pfnWSEGL_DeleteDrawable (dcontext->
-        drawable_handle);
+        dcontext->wsegl_table->
+        pfnWSEGL_DeleteDrawable (dcontext->drawable_handle);
     if (glerror) {
       GST_ELEMENT_ERROR (pvrvideosink, RESOURCE, FAILED,
           ("error deleting drawable"), ("%s", wseglstrerr (glerror)));
@@ -230,7 +257,8 @@ pvr_recreate_drawable (GstPVRVideoSink * pvrvideosink)
   }
 
   glerror =
-      dcontext->wsegl_table->pfnWSEGL_CreateWindowDrawable (dcontext->
+      dcontext->wsegl_table->
+      pfnWSEGL_CreateWindowDrawable (dcontext->gst_display_handle->
       display_handle, dcontext->glconfig, &dcontext->drawable_handle,
       (NativeWindowType) pvrvideosink->xwindow->window, &dcontext->rotation);
   if (glerror) {
@@ -248,8 +276,9 @@ pvr_get_drawable_params (GstPVRVideoSink * pvrvideosink)
   GstDrawContext *dcontext = pvrvideosink->dcontext;
 
   glerror =
-      dcontext->wsegl_table->pfnWSEGL_GetDrawableParameters (dcontext->
-      drawable_handle, &source_params, &pvrvideosink->render_params);
+      dcontext->wsegl_table->
+      pfnWSEGL_GetDrawableParameters (dcontext->drawable_handle, &source_params,
+      &pvrvideosink->render_params);
 
   if (glerror == WSEGL_BAD_DRAWABLE) {
     /* this can happen if window size changes, window is redirected/
@@ -530,6 +559,7 @@ gst_pvrvideosink_get_dcontext (GstPVRVideoSink * pvrvideosink)
   int eventBase, errorBase, major, minor;
   char *driver, *device;
   int fd = -1;
+  WSEGLDisplayHandle display_handle;
 
   dcontext = g_new0 (GstDrawContext, 1);
   dcontext->x_lock = g_mutex_new ();
@@ -577,12 +607,15 @@ gst_pvrvideosink_get_dcontext (GstPVRVideoSink * pvrvideosink)
     goto invalid_display;
 
   glerror = dcontext->wsegl_table->pfnWSEGL_InitialiseDisplay (
-      (NativeDisplayType) dcontext->x_display, &dcontext->display_handle,
+      (NativeDisplayType) dcontext->x_display, &display_handle,
       &glcaps, &dcontext->glconfig);
   if (glerror)
     goto wsegl_init_error;
 
-  displayImpl = (DRI2Display *) dcontext->display_handle;
+  dcontext->gst_display_handle =
+      gst_display_handle_new (display_handle, dcontext->wsegl_table);
+
+  displayImpl = (DRI2Display *) display_handle;
   dcontext->pvr_context = displayImpl->hContext;
 
 #if 0
@@ -946,13 +979,13 @@ gst_pvrvideosink_destroy_drawable (GstPVRVideoSink * pvrvideosink)
 {
   if (pvrvideosink->dcontext != NULL) {
     if (pvrvideosink->dcontext->drawable_handle)
-      pvrvideosink->dcontext->wsegl_table->
-          pfnWSEGL_DeleteDrawable (pvrvideosink->dcontext->drawable_handle);
+      pvrvideosink->dcontext->
+          wsegl_table->pfnWSEGL_DeleteDrawable (pvrvideosink->dcontext->
+          drawable_handle);
 
-#if 0
-    pvrvideosink->dcontext->wsegl_table->
-        pfnWSEGL_CloseDisplay (pvrvideosink->dcontext->display_handle);
-#endif
+    gst_display_handle_unref (pvrvideosink->dcontext->gst_display_handle);
+    pvrvideosink->dcontext->gst_display_handle = NULL;
+    pvrvideosink->dcontext->pvr_context = NULL;
   }
 }
 
@@ -1459,6 +1492,9 @@ import_buf:
   if (buf_priv == NULL) {
     /* FIXME: make this an error */
     GST_ERROR_OBJECT (pvrvideosink, "dropping frame");
+    if (newbuf) {
+      gst_buffer_unref (newbuf);
+    }
     return GST_FLOW_OK;
   }
 
diff --git a/sys/pvr2d/gstpvrvideosink.h b/sys/pvr2d/gstpvrvideosink.h
index 1689c93..04abc95 100644
--- a/sys/pvr2d/gstpvrvideosink.h
+++ b/sys/pvr2d/gstpvrvideosink.h
@@ -45,6 +45,7 @@ G_BEGIN_DECLS
   (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_PVRVIDEOSINK))
 #define GST_IS_PVRVIDEOSINK_CLASS(klass) \
   (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_PVRVIDEOSINK))
+typedef struct _GstDisplayHandle GstDisplayHandle;
 typedef struct _GstDrawContext GstDrawContext;
 typedef struct _GstXWindow GstXWindow;
 
@@ -54,6 +55,13 @@ typedef struct _GstPVRVideoBufferClass GstPVRVideoBufferClass;
 typedef struct _GstPVRVideoSink GstPVRVideoSink;
 typedef struct _GstPVRVideoSinkClass GstPVRVideoSinkClass;
 
+struct _GstDisplayHandle
+{
+  gint refcount;
+  const WSEGL_FunctionTable *wsegl_table;
+  WSEGLDisplayHandle display_handle;
+};
+
 struct _GstDrawContext
 {
   /* PVR2D */
@@ -72,7 +80,7 @@ struct _GstDrawContext
   /* WSEGL */
   const WSEGL_FunctionTable *wsegl_table;
 
-  WSEGLDisplayHandle display_handle;
+  GstDisplayHandle *gst_display_handle;
   const WSEGLCaps **glcaps;
   WSEGLConfig *glconfig;
   WSEGLDrawableHandle drawable_handle;
@@ -165,5 +173,8 @@ struct _GstPVRVideoSinkClass
 
 GType gst_pvrvideosink_get_type (void);
 
+GstDisplayHandle * gst_display_handle_ref (GstDisplayHandle * display_handle);
+void gst_display_handle_unref (GstDisplayHandle * display_handle);
+
 G_END_DECLS
 #endif /* __GST_PVRVIDEOSINK_H__ */
-- 
1.7.9.5


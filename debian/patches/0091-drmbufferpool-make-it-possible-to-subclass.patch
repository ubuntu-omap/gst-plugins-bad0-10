From 6f4f767875473601ef9589f4761e2a28a96b37d2 Mon Sep 17 00:00:00 2001
From: Rob Clark <rob@ti.com>
Date: Thu, 14 Jun 2012 11:01:37 -0500
Subject: [PATCH] drmbufferpool: make it possible to subclass

---
 gst-libs/gst/drm/Makefile.am        |    1 +
 gst-libs/gst/drm/drmbufferpool.h    |  120 -----------------------------------
 gst-libs/gst/drm/gstdrmbufferpool.c |   84 +++++++++++++++---------
 gst-libs/gst/drm/gstdrmbufferpool.h |   95 +++++++++++++++++++++++++++
 4 files changed, 150 insertions(+), 150 deletions(-)
 delete mode 100644 gst-libs/gst/drm/drmbufferpool.h

diff --git a/gst-libs/gst/drm/Makefile.am b/gst-libs/gst/drm/Makefile.am
index ccce728..ea1c290 100644
--- a/gst-libs/gst/drm/Makefile.am
+++ b/gst-libs/gst/drm/Makefile.am
@@ -21,6 +21,7 @@ libgstdrm_@GST_MAJORMINOR@_la_CFLAGS = \
 
 libgstdrm_@GST_MAJORMINOR@_la_LIBADD = \
 	$(DRM_LIBS) \
+	-lgstdmabuf-$(GST_MAJORMINOR) \
 	$(GST_PLUGINS_BASE_LIBS) \
 	$(GST_BASE_LIBS) \
 	$(GST_LIBS) \
diff --git a/gst-libs/gst/drm/drmbufferpool.h b/gst-libs/gst/drm/drmbufferpool.h
deleted file mode 100644
index a5bf842..0000000
--- a/gst-libs/gst/drm/drmbufferpool.h
+++ /dev/null
@@ -1,120 +0,0 @@
-/*
- * GStreamer
- *
- * Copyright (C) 2012 Texas Instruments 
- * Copyright (C) 2012 Collabora Ltd
- *
- * Authors:
- *  Alessandro Decina <alessandro.decina@collabora.co.uk>
- *  Rob Clark <rob.clark@linaro.org>
- *
- * This library is free software; you can redistribute it and/or
- * modify it under the terms of the GNU Lesser General Public
- * License as published by the Free Software Foundation
- * version 2.1 of the License.
- *
- * This library is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
- * Lesser General Public License for more details.
- *
- * You should have received a copy of the GNU Lesser General Public
- * License along with this library; if not, write to the Free Software
- * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
- */
-
-#ifndef __DRMBUFFERPOOL_H__
-#define __DRMBUFFERPOOL_H__
-
-/*
- * private header for gstdrmbufferpool
- */
-
-#include <string.h>
-#include <stdint.h>
-#include <errno.h>
-#include <string.h>
-
-#include <gst/gst.h>
-#include <gst/dmabuf/dmabuf.h>
-
-/* TODO replace dependency on libdrm_omap w/ libdrm.. the only thing
- * missing is way to allocate buffers, but this should probably be
- * done via libdrm?
- */
-#include <omap_drm.h>
-#include <omap_drmif.h>
-
-G_BEGIN_DECLS
-
-/*
- * GstDRMBuffer:
- */
-
-#define GST_TYPE_DRM_BUFFER (gst_drm_buffer_get_type())
-#define GST_IS_DRM_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_DRM_BUFFER))
-#define GST_DRM_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_DRM_BUFFER, GstDRMBuffer))
-
-typedef struct _GstDRMBuffer GstDRMBuffer;
-typedef struct _GstDRMBufferClass GstDRMBufferClass;
-
-/* forward declaration */
-struct _GstDRMBufferPool;
-
-struct _GstDRMBuffer {
-  GstBuffer parent;
-
-  struct omap_bo *bo;
-
-  struct _GstDRMBufferPool *pool; /* buffer-pool that this buffer belongs to */
-  GstDRMBuffer *next;             /* next in freelist, if not in use */
-  gboolean remove_from_pool;
-};
-
-struct _GstDRMBufferClass {
-  GstBufferClass klass;
-};
-
-GType gst_drm_buffer_get_type (void);
-GstDRMBuffer * gst_drm_buffer_new (struct _GstDRMBufferPool * pool);
-void gst_drm_buffer_set_pool (GstDRMBuffer * self, struct _GstDRMBufferPool * pool);
-
-/*
- * GstDRMBufferPool:
- */
-
-/* TODO do we want GstDRMBufferPool to be subclassed.. if so, move this
- * back to public header.. for now I'm just keeping the implementation
- * entirely opaque
- */
-struct _GstDRMBufferPool
-{
-  GstMiniObject parent;
-
-  int fd;
-  struct omap_device *dev;
-
-  /* output (padded) size including any codec padding: */
-  gint width, height;
-
-  gboolean         strided;  /* 2d buffers? */
-  GstCaps         *caps;
-  GMutex          *lock;
-  gboolean         running;  /* with lock */
-  GstElement      *element;  /* the element that owns us.. */
-  GstDRMBuffer    *head; /* list of available buffers */
-  GstDRMBuffer    *tail;
-  guint size;
-};
-
-struct _GstDRMBufferPoolClass
-{
-  GstMiniObjectClass klass;
-};
-
-gboolean gst_drm_buffer_pool_put (GstDRMBufferPool * self, GstDRMBuffer * buf);
-
-
-G_END_DECLS
-
-#endif /* __DRMBUFFERPOOL_H__ */
diff --git a/gst-libs/gst/drm/gstdrmbufferpool.c b/gst-libs/gst/drm/gstdrmbufferpool.c
index 58fbb11..4382fc9 100644
--- a/gst-libs/gst/drm/gstdrmbufferpool.c
+++ b/gst-libs/gst/drm/gstdrmbufferpool.c
@@ -27,26 +27,27 @@
 #include "config.h"
 #endif
 
+#include <string.h>
+
+#include <gst/dmabuf/dmabuf.h>
+
 #include "gstdrmbufferpool.h"
-#include "drmbufferpool.h"
+
+static GstDRMBuffer * gst_drm_buffer_new (GstDRMBufferPool * pool);
+static void gst_drm_buffer_set_pool (GstDRMBuffer * self,
+    GstDRMBufferPool * pool);
 
 /*
  * GstDRMBufferPool:
  */
 
-typedef GstDRMBufferPool GstDRMDRMBufferPool;
-typedef GstDRMBufferPoolClass GstDRMDRMBufferPoolClass;
-
-G_DEFINE_TYPE (GstDRMDRMBufferPool, gst_drm_buffer_pool,
+G_DEFINE_TYPE (GstDRMBufferPool, gst_drm_buffer_pool,
     GST_TYPE_MINI_OBJECT);
 
-GstDRMBufferPool *
-gst_drm_buffer_pool_new (GstElement * element,
-    int fd, GstCaps * caps, guint size)
+void
+gst_drm_buffer_pool_initialize (GstDRMBufferPool * self,
+    GstElement * element, int fd, GstCaps * caps, guint size)
 {
-  GstDRMBufferPool *self = (GstDRMBufferPool *)
-      gst_mini_object_new (GST_TYPE_DRM_BUFFER_POOL);
-
   self->element = gst_object_ref (element);
   self->fd   = fd;
   self->dev  = omap_device_new (fd);
@@ -57,6 +58,16 @@ gst_drm_buffer_pool_new (GstElement * element,
   self->tail = NULL;
   self->lock = g_mutex_new ();
   self->running = TRUE;
+}
+
+GstDRMBufferPool *
+gst_drm_buffer_pool_new (GstElement * element,
+    int fd, GstCaps * caps, guint size)
+{
+  GstDRMBufferPool *self = (GstDRMBufferPool *)
+      gst_mini_object_new (GST_TYPE_DRM_BUFFER_POOL);
+
+  gst_drm_buffer_pool_initialize (self, element, fd, caps, size);
 
   return self;
 }
@@ -80,6 +91,7 @@ gst_drm_buffer_pool_set_caps (GstDRMBufferPool * self, GstCaps * caps)
 
     gst_structure_get_int (s, "width", &self->width);
     gst_structure_get_int (s, "height", &self->height);
+    gst_structure_get_fourcc (s, "format", &self->fourcc);
   } else {
     self->width = 0;
     self->height = 0;
@@ -151,7 +163,7 @@ gst_drm_buffer_pool_get (GstDRMBufferPool * self, gboolean force_alloc)
       if (self->head == NULL)
         self->tail = NULL;
     } else {
-      buf = gst_drm_buffer_new (self);
+      buf = GST_DRM_BUFFER_POOL_GET_CLASS (self)->buffer_alloc (self);
     }
     if (self->caps)
       gst_buffer_set_caps (GST_BUFFER (buf), self->caps);
@@ -163,7 +175,7 @@ gst_drm_buffer_pool_get (GstDRMBufferPool * self, gboolean force_alloc)
   return GST_BUFFER (buf);
 }
 
-gboolean
+static gboolean
 gst_drm_buffer_pool_put (GstDRMBufferPool * self, GstDRMBuffer * buf)
 {
   gboolean reuse = FALSE;
@@ -209,7 +221,8 @@ static void
 gst_drm_buffer_pool_class_init (GstDRMBufferPoolClass * klass)
 {
   GstMiniObjectClass *mini_object_class = GST_MINI_OBJECT_CLASS (klass);
-
+  klass->buffer_alloc =
+      GST_DEBUG_FUNCPTR (gst_drm_buffer_new);
   mini_object_class->finalize = (GstMiniObjectFinalizeFunction)
       GST_DEBUG_FUNCPTR (gst_drm_buffer_pool_finalize);
 }
@@ -223,13 +236,28 @@ gst_drm_buffer_pool_init (GstDRMBufferPool * self)
  * GstDRMBuffer:
  */
 
+G_DEFINE_TYPE (GstDRMBuffer, gst_drm_buffer, GST_TYPE_BUFFER);
 
-typedef GstDRMBuffer GstDRMDRMBuffer;
-typedef GstDRMBufferClass GstDRMDRMBufferClass;
+void
+gst_drm_buffer_initialize (GstDRMBuffer * self,
+    GstDRMBufferPool * pool, struct omap_bo * bo)
+{
+  self->bo = bo;
 
-G_DEFINE_TYPE (GstDRMDRMBuffer, gst_drm_buffer, GST_TYPE_BUFFER);
+  GST_BUFFER_DATA (self) = omap_bo_map (self->bo);
+  GST_BUFFER_SIZE (self) = pool->size;
+
+  /* attach dmabuf handle to buffer so that elements from other
+   * plugins can access for zero copy hw accel:
+   */
+  // XXX buffer doesn't take ownership of the GstDmaBuf...
+  gst_buffer_set_dma_buf (GST_BUFFER (self),
+      gst_dma_buf_new (omap_bo_dmabuf (self->bo)));
+
+  gst_drm_buffer_set_pool (self, pool);
+}
 
-GstDRMBuffer *
+static GstDRMBuffer *
 gst_drm_buffer_new (GstDRMBufferPool * pool)
 {
   GstDRMBuffer *self = (GstDRMBuffer *)
@@ -240,23 +268,14 @@ gst_drm_buffer_new (GstDRMBufferPool * pool)
    * otherwise we might want some support for various different
    * drm drivers here:
    */
-  self->bo = omap_bo_new (pool->dev, pool->size, OMAP_BO_WC);
-
-  GST_BUFFER_DATA (self) = omap_bo_map (self->bo);
-  GST_BUFFER_SIZE (self) = pool->size;
-
-  /* attach dmabuf handle to buffer so that elements from other
-   * plugins can access for zero copy hw accel:
-   */
-  gst_buffer_set_dma_buf (GST_BUFFER (self),
-      gst_dma_buf_new (omap_bo_dmabuf (self->bo)));
+  struct omap_bo *bo = omap_bo_new (pool->dev, pool->size, OMAP_BO_WC);
 
-  gst_drm_buffer_set_pool (self, pool);
+  gst_drm_buffer_initialize (self, pool, bo);
 
   return self;
 }
 
-void
+static void
 gst_drm_buffer_set_pool (GstDRMBuffer * self, GstDRMBufferPool * pool)
 {
 
@@ -282,6 +301,11 @@ gst_drm_buffer_finalize (GstDRMBuffer * self)
   if (resuscitated)
     return;
 
+  if (GST_DRM_BUFFER_POOL_GET_CLASS (self->pool)->buffer_cleanup) {
+    GST_DRM_BUFFER_POOL_GET_CLASS (self->pool)->buffer_cleanup (
+        self->pool, self);
+  }
+
   GST_BUFFER_DATA (self) = NULL;
   omap_bo_del (self->bo);
 
diff --git a/gst-libs/gst/drm/gstdrmbufferpool.h b/gst-libs/gst/drm/gstdrmbufferpool.h
index b7d09ed..4d4707c 100644
--- a/gst-libs/gst/drm/gstdrmbufferpool.h
+++ b/gst-libs/gst/drm/gstdrmbufferpool.h
@@ -30,9 +30,23 @@
 
 G_BEGIN_DECLS
 
+/* TODO replace dependency on libdrm_omap w/ libdrm.. the only thing
+ * missing is way to allocate buffers, but this should probably be
+ * done via libdrm?
+ *
+ * NOTE: this dependency is only for those who want to subclass us,
+ * so we could perhaps move the struct definitions into a separate
+ * header or split out private ptr and move that into the .c file..
+ */
+#include <stdint.h>
+#include <omap_drm.h>
+#include <omap_drmif.h>
+
 #define GST_TYPE_DRM_BUFFER_POOL (gst_drm_buffer_pool_get_type())
 #define GST_IS_DRM_BUFFER_POOL(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_DRM_BUFFER_POOL))
 #define GST_DRM_BUFFER_POOL(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_DRM_BUFFER_POOL, GstDRMBufferPool))
+#define GST_DRM_BUFFER_POOL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_DRM_BUFFER_POOL, GstDRMBufferPoolClass))
+#define GST_DRM_BUFFER_POOL_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_DRM_BUFFER_POOL, GstDRMBufferPoolClass))
 
 #define GST_DRM_BUFFER_POOL_LOCK(self)     g_mutex_lock ((self)->lock)
 #define GST_DRM_BUFFER_POOL_UNLOCK(self)   g_mutex_unlock ((self)->lock)
@@ -40,7 +54,61 @@ G_BEGIN_DECLS
 typedef struct _GstDRMBufferPool GstDRMBufferPool;
 typedef struct _GstDRMBufferPoolClass GstDRMBufferPoolClass;
 
+#define GST_TYPE_DRM_BUFFER (gst_drm_buffer_get_type())
+#define GST_IS_DRM_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_DRM_BUFFER))
+#define GST_DRM_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_DRM_BUFFER, GstDRMBuffer))
+
+typedef struct _GstDRMBuffer GstDRMBuffer;
+typedef struct _GstDRMBufferClass GstDRMBufferClass;
+
+/*
+ * GstDRMBufferPool:
+ */
+
+struct _GstDRMBufferPool {
+  GstMiniObject parent;
+
+  int fd;
+  struct omap_device *dev;
+
+  /* output (padded) size including any codec padding: */
+  gint width, height;
+  guint32 fourcc;
+
+  gboolean         strided;  /* 2d buffers? */
+  GstCaps         *caps;
+  GMutex          *lock;
+  gboolean         running;  /* with lock */
+  GstElement      *element;  /* the element that owns us.. */
+  GstDRMBuffer    *head; /* list of available buffers */
+  GstDRMBuffer    *tail;
+  guint size;
+
+  /* TODO add reserved */
+};
+
+struct _GstDRMBufferPoolClass {
+  GstMiniObjectClass klass;
+
+  /* allow the subclass to allocate it's own buffers that extend
+   * GstDRMBuffer:
+   */
+  GstDRMBuffer * (*buffer_alloc)(GstDRMBufferPool * pool);
+
+  /* The a buffer subclass should not override finalize, as that
+   * would interfere with reviving the buffer and returning to the
+   * pool.  Instead you can implement this vmethod to cleanup a
+   * buffer.
+   */
+  void (*buffer_cleanup)(GstDRMBufferPool * pool, GstDRMBuffer *buf);
+
+  /* TODO add reserved */
+};
+
 GType gst_drm_buffer_pool_get_type (void);
+
+void gst_drm_buffer_pool_initialize (GstDRMBufferPool * self,
+    GstElement * element, int fd, GstCaps * caps, guint size);
 GstDRMBufferPool * gst_drm_buffer_pool_new (GstElement * element,
     int fd, GstCaps * caps, guint size);
 void gst_drm_buffer_pool_destroy (GstDRMBufferPool * self);
@@ -51,6 +119,33 @@ gboolean gst_drm_buffer_pool_check_caps (GstDRMBufferPool * self,
 GstBuffer * gst_drm_buffer_pool_get (GstDRMBufferPool * self,
     gboolean force_alloc);
 
+
+/*
+ * GstDRMBuffer:
+ */
+
+struct _GstDRMBuffer {
+  GstBuffer parent;
+
+  struct omap_bo *bo;
+
+  GstDRMBufferPool *pool;    /* buffer-pool that this buffer belongs to */
+  GstDRMBuffer *next;        /* next in freelist, if not in use */
+  gboolean remove_from_pool;
+
+  /* TODO add reserved */
+};
+
+struct _GstDRMBufferClass {
+  GstBufferClass klass;
+  /* TODO add reserved */
+};
+
+GType gst_drm_buffer_get_type (void);
+
+void gst_drm_buffer_initialize (GstDRMBuffer * self,
+    GstDRMBufferPool * pool, struct omap_bo * bo);
+
 G_END_DECLS
 
 #endif /* __GSTDRMBUFFERPOOL_H__ */
-- 
1.7.9.5


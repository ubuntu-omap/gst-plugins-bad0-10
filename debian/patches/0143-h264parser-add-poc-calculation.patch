From a04a9f131a2feca3f35fdb946e6efbf3a0615a88 Mon Sep 17 00:00:00 2001
From: Vincent Penquerc'h <vincent.penquerch@collabora.co.uk>
Date: Thu, 4 Oct 2012 12:25:30 +0100
Subject: [PATCH] h264parser: add poc calculation

Only for type 0 at the moment, which is what the sample I have
uses.
---
 gst-libs/gst/codecparsers/gsth264parser.c |  173 ++++++++++++++++++++++++++++-
 gst-libs/gst/codecparsers/gsth264parser.h |   27 +++++
 2 files changed, 197 insertions(+), 3 deletions(-)

diff --git a/gst-libs/gst/codecparsers/gsth264parser.c b/gst-libs/gst/codecparsers/gsth264parser.c
index 572a28d..6ee9f3a 100644
--- a/gst-libs/gst/codecparsers/gsth264parser.c
+++ b/gst-libs/gst/codecparsers/gsth264parser.c
@@ -93,6 +93,9 @@
 GST_DEBUG_CATEGORY (h264_parser_debug);
 #define GST_CAT_DEFAULT h264_parser_debug
 
+#define POC_DATA_CURRENT 0
+#define POC_DATA_LAST_REF 1
+
 /**** Default scaling_lists according to Table 7-2 *****/
 static const guint8 default_4x4_intra[16] = {
   6, 13, 13, 20, 20, 20, 28, 28, 28, 28, 32, 32,
@@ -802,8 +805,8 @@ slice_parse_ref_pic_list_modification (GstH264SliceHdr * slice, NalReader * nr)
 }
 
 static gboolean
-gst_h264_slice_parse_dec_ref_pic_marking (GstH264SliceHdr * slice,
-    GstH264NalUnit * nalu, NalReader * nr)
+gst_h264_slice_parse_dec_ref_pic_marking (GstH264NalParser * nalparser,
+    GstH264SliceHdr * slice, GstH264NalUnit * nalu, NalReader * nr)
 {
   GstH264DecRefPicMarking *dec_ref_pic_m;
 
@@ -811,6 +814,11 @@ gst_h264_slice_parse_dec_ref_pic_marking (GstH264SliceHdr * slice,
 
   dec_ref_pic_m = &slice->dec_ref_pic_marking;
 
+  /* We want to remember if the last reference frame to be decoded had
+     a memory management control operation of 5, as this is used to
+     determine picture order count. */
+  nalparser->poc_data[POC_DATA_CURRENT].seen_mmco_5 = FALSE;
+
   if (nalu->idr_pic_flag) {
     READ_UINT8 (nr, dec_ref_pic_m->no_output_of_prior_pics_flag, 1);
     READ_UINT8 (nr, dec_ref_pic_m->long_term_reference_flag, 1);
@@ -843,6 +851,9 @@ gst_h264_slice_parse_dec_ref_pic_marking (GstH264SliceHdr * slice,
         if (mem_mgmt_ctrl_op == 4)
           READ_UE (nr, refpicmarking->max_long_term_frame_idx_plus1);
 
+        if (mem_mgmt_ctrl_op == 5)
+          nalparser->poc_data[POC_DATA_CURRENT].seen_mmco_5 = TRUE;
+
         dec_ref_pic_m->n_ref_pic_marking++;
       }
     }
@@ -1740,6 +1751,15 @@ gst_h264_parser_parse_slice_hdr (GstH264NalParser * nalparser,
     return GST_H264_PARSER_ERROR;
   }
 
+  /* When starting a new slice header, we want to move the POC data
+     for the last frame, if it was a reference frame, to the "previous
+     reference frame" slot. */
+  if (nalparser->poc_data[POC_DATA_CURRENT].is_reference) {
+    memcpy (nalparser->poc_data + POC_DATA_LAST_REF, nalparser->poc_data +
+        POC_DATA_CURRENT, sizeof (nalparser->poc_data[0]));
+    memset (nalparser->poc_data + POC_DATA_CURRENT, 0,
+        sizeof (nalparser->poc_data[0]));
+  }
 
   nal_reader_init (&nr, nalu->data + nalu->offset + 1, nalu->size - 1);
 
@@ -1789,6 +1809,8 @@ gst_h264_parser_parse_slice_hdr (GstH264NalParser * nalparser,
     if (slice->field_pic_flag)
       READ_UINT8 (&nr, slice->bottom_field_flag, 1);
   }
+  nalparser->poc_data[POC_DATA_CURRENT].bottom_field_flag =
+      slice->bottom_field_flag;
 
   /* calculate MaxPicNum */
   if (slice->field_pic_flag)
@@ -1844,7 +1866,8 @@ gst_h264_parser_parse_slice_hdr (GstH264NalParser * nalparser,
   }
 
   if (nalu->ref_idc != 0) {
-    if (!gst_h264_slice_parse_dec_ref_pic_marking (slice, nalu, &nr))
+    nalparser->poc_data[POC_DATA_CURRENT].is_reference = TRUE;
+    if (!gst_h264_slice_parse_dec_ref_pic_marking (nalparser, slice, nalu, &nr))
       goto error;
   }
 
@@ -1890,6 +1913,150 @@ error:
   return GST_H264_PARSER_ERROR;
 }
 
+static GstH264ParserResult
+gst_h264_parser_get_poc_0 (GstH264NalParser * nalparser,
+    GstH264NalUnit * nalu, GstH264SliceHdr * slice,
+    guint * TopFieldOrderCnt, guint * BottomFieldOrderCnt)
+{
+  guint prevPicOrderCntMsb, prevPicOrderCntLsb;
+  guint MaxPicOrderCntLsb, pic_order_cnt_lsb;
+  GstH264SPS *sps = nalparser->last_sps;
+  guint PicOrderCntMsb;
+  const GstH264POCData *prev_poc_data = nalparser->poc_data + POC_DATA_LAST_REF;
+
+  /* H264 spec 8.2.1.1 */
+  GST_DEBUG ("case 0");
+
+  if (slice->type == GST_H264_NAL_SLICE_IDR) {
+    prevPicOrderCntMsb = 0;
+    prevPicOrderCntLsb = 0;
+  } else {
+    if (prev_poc_data->seen_mmco_5) {
+      if (!prev_poc_data->bottom_field_flag) {
+        prevPicOrderCntMsb = 0;
+        prevPicOrderCntLsb = prev_poc_data->TopFieldOrderCnt;
+      } else {
+        prevPicOrderCntMsb = 0;
+        prevPicOrderCntLsb = 0;
+      }
+    } else {
+      prevPicOrderCntMsb = prev_poc_data->PicOrderCntMsb;
+      prevPicOrderCntLsb = prev_poc_data->pic_order_cnt_lsb;
+    }
+  }
+
+  MaxPicOrderCntLsb = 1 << (sps->log2_max_pic_order_cnt_lsb_minus4 + 4);
+  pic_order_cnt_lsb = slice->pic_order_cnt_lsb;
+  if ((pic_order_cnt_lsb < prevPicOrderCntLsb) &&
+      ((prevPicOrderCntLsb - pic_order_cnt_lsb) >= (MaxPicOrderCntLsb / 2)))
+    PicOrderCntMsb = prevPicOrderCntMsb + MaxPicOrderCntLsb;
+  else if ((pic_order_cnt_lsb > prevPicOrderCntLsb) &&
+      ((pic_order_cnt_lsb - prevPicOrderCntLsb) > (MaxPicOrderCntLsb / 2)))
+    PicOrderCntMsb = prevPicOrderCntMsb - MaxPicOrderCntLsb;
+  else
+    PicOrderCntMsb = prevPicOrderCntMsb;
+  nalparser->poc_data[POC_DATA_CURRENT].PicOrderCntMsb = PicOrderCntMsb;
+
+  if (!slice->field_pic_flag || !slice->bottom_field_flag) {
+    *TopFieldOrderCnt = PicOrderCntMsb + pic_order_cnt_lsb;
+  }
+  if (!slice->field_pic_flag) {
+    *BottomFieldOrderCnt =
+        *TopFieldOrderCnt + slice->delta_pic_order_cnt_bottom;
+  } else if (slice->bottom_field_flag) {
+    *BottomFieldOrderCnt = PicOrderCntMsb + pic_order_cnt_lsb;
+  }
+
+  return GST_H264_PARSER_OK;
+}
+
+static GstH264ParserResult
+gst_h264_parser_get_poc_1 (GstH264NalParser * nalparser,
+    GstH264NalUnit * nalu, GstH264SliceHdr * slice,
+    guint * TopFieldOrderCnt, guint * BottomFieldOrderCnt)
+{
+  /* H264 spec 8.2.1.2 */
+  GST_DEBUG ("case 1");
+
+  return GST_H264_PARSER_ERROR;
+}
+
+static GstH264ParserResult
+gst_h264_parser_get_poc_2 (GstH264NalParser * nalparser,
+    GstH264NalUnit * nalu, GstH264SliceHdr * slice,
+    guint * TopFieldOrderCnt, guint * BottomFieldOrderCnt)
+{
+  /* H264 spec 8.2.1.3 */
+  GST_DEBUG ("case 2");
+
+  return GST_H264_PARSER_ERROR;
+}
+
+/**
+ * gst_h264_parser_get_poc:
+ * @nalparser: a #GstH264NalParser
+ * @nalu: The #GST_H264_NAL_SEI #GstH264NalUnit to parse
+ *
+ * Parses @data, and fills @poc.
+ *
+ * Returns: a #GstH264ParserResult
+ */
+GstH264ParserResult
+gst_h264_parser_get_poc (GstH264NalParser * nalparser,
+    GstH264NalUnit * nalu, GstH264SliceHdr * slice, guint * poc)
+{
+  GstH264SPS *sps;
+  guint TopFieldOrderCnt, BottomFieldOrderCnt;
+  GstH264ParserResult res;
+
+  GST_DEBUG ("start");
+
+  if (!nalparser->last_sps || !nalparser->last_sps->valid) {
+    GST_WARNING ("didn't get the associated sequence paramater set for the "
+        "current access unit");
+    return GST_H264_PARSER_ERROR;
+  }
+
+  sps = nalparser->last_sps;
+  switch (sps->pic_order_cnt_type) {
+    case 2:
+      res = gst_h264_parser_get_poc_2 (nalparser, nalu, slice,
+          &TopFieldOrderCnt, &BottomFieldOrderCnt);
+      break;
+    case 1:
+      res = gst_h264_parser_get_poc_1 (nalparser, nalu, slice,
+          &TopFieldOrderCnt, &BottomFieldOrderCnt);
+      break;
+    case 0:
+      res = gst_h264_parser_get_poc_0 (nalparser, nalu, slice,
+          &TopFieldOrderCnt, &BottomFieldOrderCnt);
+      break;
+    default:
+      GST_WARNING ("Wrong pic_order_cnt_type: %d", sps->pic_order_cnt_type);
+      return GST_H264_PARSER_ERROR;
+  }
+
+  if (res != GST_H264_PARSER_OK)
+    return res;
+
+  if (!slice->field_pic_flag) {
+    *poc =
+        TopFieldOrderCnt <
+        BottomFieldOrderCnt ? TopFieldOrderCnt : BottomFieldOrderCnt;
+  } else if (!slice->bottom_field_flag) {
+    *poc = TopFieldOrderCnt;
+  } else {
+    *poc = BottomFieldOrderCnt;
+  }
+
+  GST_DEBUG ("Got TopFieldOrderCnt %u, BottomFieldOrderCnt %u, poc %u",
+      TopFieldOrderCnt, BottomFieldOrderCnt, *poc);
+
+  nalu->poc = *poc;
+
+  return GST_H264_PARSER_OK;
+}
+
 /**
  * gst_h264_parser_parse_sei:
  * @nalparser: a #GstH264NalParser
diff --git a/gst-libs/gst/codecparsers/gsth264parser.h b/gst-libs/gst/codecparsers/gsth264parser.h
index 3c22156..9f467ba 100644
--- a/gst-libs/gst/codecparsers/gsth264parser.h
+++ b/gst-libs/gst/codecparsers/gsth264parser.h
@@ -188,6 +188,8 @@ typedef struct _GstH264PicTiming              GstH264PicTiming;
 typedef struct _GstH264BufferingPeriod        GstH264BufferingPeriod;
 typedef struct _GstH264SEIMessage             GstH264SEIMessage;
 
+typedef struct _GstH264POCData                GstH264POCData;
+
 /**
  * GstH264NalUnit:
  * @ref_idc: not equal to 0 specifies that the content of the NAL unit contains a sequence
@@ -216,6 +218,7 @@ struct _GstH264NalUnit
   guint offset;
   guint sc_offset;
   gboolean valid;
+  guint poc;
 
   guint8 *data;
 };
@@ -660,6 +663,20 @@ struct _GstH264SEIMessage
   };
 };
 
+/* Note: Where the spec has a name for a value, the following
+   structure uses that name. This explains the capitalization
+   weirdness. It helps me find my way in the spec, so would be
+   appreciated if it could stay that way. */
+struct _GstH264POCData
+{
+  guint pic_order_cnt_lsb;
+  guint PicOrderCntMsb;
+  guint TopFieldOrderCnt;
+  gboolean seen_mmco_5;
+  gboolean bottom_field_flag;
+  gboolean is_reference;
+};
+
 /**
  * GstH264NalParser:
  *
@@ -672,6 +689,14 @@ struct _GstH264NalParser
   GstH264PPS pps[GST_H264_MAX_PPS_COUNT];
   GstH264SPS *last_sps;
   GstH264PPS *last_pps;
+
+  /* Determining the picture order count of pictures requires
+     remembering a few things about the last reference picture,
+     so we keep that info for the currently decoded picture,
+     and for last decoded picture. [0] is what is being decoded,
+     and it will moved to 1 when starting decoding a new picture
+     if that picture was a reference. */
+  GstH264POCData poc_data[2];
 };
 
 GstH264NalParser *gst_h264_nal_parser_new             (void);
@@ -711,6 +736,8 @@ GstH264ParserResult gst_h264_parse_sps                (GstH264NalUnit *nalu,
 
 GstH264ParserResult gst_h264_parse_pps                (GstH264NalParser *nalparser,
                                                        GstH264NalUnit *nalu, GstH264PPS *pps);
+GstH264ParserResult gst_h264_parser_get_poc           (GstH264NalParser * nalparser,
+                                                       GstH264NalUnit * nalu, GstH264SliceHdr * slice, guint *poc);
 
 G_END_DECLS
 #endif
-- 
1.7.9.5


From 741977e00ec9c758d8befd270f3730255a6c0421 Mon Sep 17 00:00:00 2001
From: Vincent Penquerc'h <vincent.penquerch@collabora.co.uk>
Date: Fri, 19 Oct 2012 17:01:45 +0100
Subject: [PATCH] codecparsers: use the new start code scanner where
 appropriate

---
 gst-libs/gst/codecparsers/gsth264parser.c      |   27 ++++++------
 gst-libs/gst/codecparsers/gstmpeg4parser.c     |   30 +-------------
 gst-libs/gst/codecparsers/gstmpegvideoparser.c |   53 +-----------------------
 gst-libs/gst/codecparsers/parserutils.c        |   27 ++++++++++++
 gst-libs/gst/codecparsers/parserutils.h        |    2 +
 5 files changed, 46 insertions(+), 93 deletions(-)

diff --git a/gst-libs/gst/codecparsers/gsth264parser.c b/gst-libs/gst/codecparsers/gsth264parser.c
index d9a35e1..3fae854 100644
--- a/gst-libs/gst/codecparsers/gsth264parser.c
+++ b/gst-libs/gst/codecparsers/gsth264parser.c
@@ -88,6 +88,7 @@
 
 #include <gst/base/gstbytereader.h>
 #include <gst/base/gstbitreader.h>
+#include "parserutils.h"
 #include <string.h>
 
 GST_DEBUG_CATEGORY (h264_parser_debug);
@@ -355,6 +356,15 @@ nal_reader_get_se (NalReader * nr, gint32 * val)
   return TRUE;
 }
 
+/* parserutils defines the same macros, and there are so many calls
+   to these here and elsewhere that it seems best to avoid thrashing
+   the history by renaming one set in lots of places. Undef for now. */
+#undef CHECK_ALLOWED
+#undef READ_UINT8
+#undef READ_UINT16
+#undef READ_UINT32
+#undef READ_UINT64
+
 #define CHECK_ALLOWED(val, min, max) { \
   if (val < min || val > max) { \
     GST_WARNING ("value not in allowed range. value: %d, range %d-%d", \
@@ -386,7 +396,7 @@ nal_reader_get_se (NalReader * nr, gint32 * val)
 
 #define READ_UINT64(nr, val, nbits) { \
   if (!nal_reader_get_bits_uint64 (nr, &val, nbits)) { \
-    GST_WARNING ("failed to read uint32, nbits: %d", nbits); \
+    GST_WARNING ("failed to read uint64, nbits: %d", nbits); \
     goto error; \
   } \
 }
@@ -462,17 +472,6 @@ set_nalu_datas (GstH264NalUnit * nalu)
   GST_DEBUG ("Nal type %u, ref_idc %u", nalu->type, nalu->ref_idc);
 }
 
-static inline gint
-scan_for_start_codes (const guint8 * data, guint size)
-{
-  GstByteReader br;
-  gst_byte_reader_init (&br, data, size);
-
-  /* NALU not empty, so we can at least expect 1 (even 2) bytes following sc */
-  return gst_byte_reader_masked_scan_uint32 (&br, 0xffffff00, 0x00000100,
-      0, size);
-}
-
 static gboolean
 gst_h264_parser_more_data (NalReader * nr)
 {
@@ -1198,7 +1197,7 @@ gst_h264_parser_identify_nalu_unchecked (GstH264NalParser * nalparser,
     return GST_H264_PARSER_ERROR;
   }
 
-  off1 = scan_for_start_codes (data + offset, size - offset);
+  off1 = find_mpeg_start_code (data + offset, 0, size - offset);
 
   if (off1 < 0) {
     GST_DEBUG ("No start code prefix in this buffer");
@@ -1261,7 +1260,7 @@ gst_h264_parser_identify_nalu (GstH264NalParser * nalparser,
   if (res != GST_H264_PARSER_OK || nalu->size == 0)
     goto beach;
 
-  off2 = scan_for_start_codes (data + nalu->offset, size - nalu->offset);
+  off2 = find_mpeg_start_code (data + nalu->offset, 0, size - nalu->offset);
   if (off2 < 0) {
     GST_DEBUG ("Nal start %d, No end found", nalu->offset);
 
diff --git a/gst-libs/gst/codecparsers/gstmpeg4parser.c b/gst-libs/gst/codecparsers/gstmpeg4parser.c
index 0827e9f..7b566dc 100644
--- a/gst-libs/gst/codecparsers/gstmpeg4parser.c
+++ b/gst-libs/gst/codecparsers/gstmpeg4parser.c
@@ -407,32 +407,6 @@ gst_mpeg4_next_resync (GstMpeg4Packet * packet,
 
 /********** API **********/
 
-static guint
-find_start_code (const guint8 * data, guint offset, guint size)
-{
-  const guint8 *start = data;
-
-  g_return_val_if_fail (size > 0, -1);
-
-  /* we can't find the pattern with less than 4 bytes */
-  if (G_UNLIKELY (size < 4))
-    return -1;
-
-  data += offset;
-  size -= 3;
-  while (size > 0) {
-    const guint8 *ptr = memchr (data, 0, size);
-    if (G_UNLIKELY (ptr == NULL))
-      return -1;
-    if (ptr[2] == 1 && ptr[1] == 0)
-      return ptr - start;
-    size -= (ptr - data) + 1;
-    data = ptr + 1;
-  }
-
-  return -1;
-}
-
 /**
  * gst_mpeg4_parse:
  * @packet: The #GstMpeg4Packet to fill
@@ -487,7 +461,7 @@ gst_mpeg4_parse (GstMpeg4Packet * packet, gboolean skip_user_data,
     first_resync_marker = TRUE;
   }
 
-  off1 = find_start_code (br.data + br.byte, offset, size - offset);
+  off1 = find_mpeg_start_code (br.data + br.byte, offset, size - offset);
 
   if (off1 == -1) {
     GST_DEBUG ("No start code prefix in this buffer");
@@ -505,7 +479,7 @@ gst_mpeg4_parse (GstMpeg4Packet * packet, gboolean skip_user_data,
   packet->type = (GstMpeg4StartCode) (data[off1 + 3]);
 
 find_end:
-  off2 = find_start_code (br.data + br.byte, off1 + 4, size - off1 - 4);
+  off2 = find_mpeg_start_code (br.data + br.byte, off1 + 4, size - off1 - 4);
 
   if (off2 == -1) {
     GST_DEBUG ("Packet start %d, No end found", off1 + 4);
diff --git a/gst-libs/gst/codecparsers/gstmpegvideoparser.c b/gst-libs/gst/codecparsers/gstmpegvideoparser.c
index b725d9d..ad79d2a 100644
--- a/gst-libs/gst/codecparsers/gstmpegvideoparser.c
+++ b/gst-libs/gst/codecparsers/gstmpegvideoparser.c
@@ -230,55 +230,6 @@ failed:
   }
 }
 
-static inline guint
-scan_for_start_codes (const GstByteReader * reader, guint offset, guint size)
-{
-  const guint8 *data;
-  guint32 state;
-  guint i;
-
-  g_return_val_if_fail (size > 0, -1);
-  g_return_val_if_fail ((guint64) offset + size <= reader->size - reader->byte,
-      -1);
-
-  /* we can't find the pattern with less than 4 bytes */
-  if (G_UNLIKELY (size < 4))
-    return -1;
-
-  data = reader->data + reader->byte + offset;
-
-  /* set the state to something that does not match */
-  state = 0xffffffff;
-
-  /* now find data */
-  for (i = 0; i < size; i++) {
-    /* throw away one byte and move in the next byte */
-    state = ((state << 8) | data[i]);
-    if (G_UNLIKELY ((state & 0xffffff00) == 0x00000100)) {
-      /* we have a match but we need to have skipped at
-       * least 4 bytes to fill the state. */
-      if (G_LIKELY (i >= 3))
-        return offset + i - 3;
-    }
-
-    /* TODO: reimplement making 010001 not detected as a sc
-     * Accelerate search for start code
-     * if (data[i] > 1) {
-     * while (i < (size - 4) && data[i] > 1) {
-     *   if (data[i + 3] > 1)
-     *     i += 4;
-     *   else
-     *     i += 1;
-     * }
-     * state = 0x00000100;
-     *}
-     */
-  }
-
-  /* nothing found */
-  return -1;
-}
-
 /****** API *******/
 
 /**
@@ -314,7 +265,7 @@ gst_mpeg_video_parse (const guint8 * data, gsize size, guint offset)
 
   gst_byte_reader_init (&br, &data[offset], size);
 
-  off = scan_for_start_codes (&br, 0, size);
+  off = find_mpeg_start_code (br.data + br.byte, 0, size);
 
   if (off < 0) {
     GST_DEBUG ("No start code prefix in this buffer");
@@ -337,7 +288,7 @@ gst_mpeg_video_parse (const guint8 * data, gsize size, guint offset)
       break;
     }
 
-    off = scan_for_start_codes (&br, 0, rsize);
+    off = find_mpeg_start_code (br.data + br.byte, 0, rsize);
 
     codoffsize->size = off;
 
diff --git a/gst-libs/gst/codecparsers/parserutils.c b/gst-libs/gst/codecparsers/parserutils.c
index a31fe48..1c907df 100644
--- a/gst-libs/gst/codecparsers/parserutils.c
+++ b/gst-libs/gst/codecparsers/parserutils.c
@@ -19,6 +19,7 @@
  * Boston, MA 02111-1307, USA.
  */
 
+#include <string.h>
 #include "parserutils.h"
 
 gboolean
@@ -55,3 +56,29 @@ failed:
     return FALSE;
   }
 }
+
+guint
+find_mpeg_start_code (const guint8 * data, guint offset, guint size)
+{
+  const guint8 *start = data;
+
+  g_return_val_if_fail (size > 0, -1);
+
+  /* we can't find the pattern with less than 4 bytes */
+  if (G_UNLIKELY (size < 4))
+    return -1;
+
+  data += offset;
+  size -= 3;
+  while (size > 0) {
+    const guint8 *ptr = memchr (data, 0, size);
+    if (G_UNLIKELY (ptr == NULL))
+      return -1;
+    if (ptr[2] == 1 && ptr[1] == 0)
+      return ptr - start;
+    size -= (ptr - data) + 1;
+    data = ptr + 1;
+  }
+
+  return -1;
+}
diff --git a/gst-libs/gst/codecparsers/parserutils.h b/gst-libs/gst/codecparsers/parserutils.h
index 009b250..2a66fec 100644
--- a/gst-libs/gst/codecparsers/parserutils.h
+++ b/gst-libs/gst/codecparsers/parserutils.h
@@ -104,5 +104,7 @@ struct _VLCTable
 gboolean
 decode_vlc (GstBitReader * br, guint * res, const VLCTable * table,
     guint length);
+guint
+find_mpeg_start_code (const guint8 * data, guint offset, guint size);
 
 #endif /* __PARSER_UTILS__ */
-- 
1.7.9.5

